export default {
  getSalles: () => {
    return {
      ok: true,
      data: require('../Fixtures/salles.json')
    }
  },
  sendSuggestion:(text,categorie)=>{
    return new Promise(function(resolve, reject) {
      setTimeout(function () {
        let success=false
        console.tron.log(typeof categorie)
        if (categorie == 4) {
          resolve({
            ok: true
          });
        }
        else{
          resolve({
            ok: false
          })
        }
      }, 1500);
    });

  },
  getHoraires: () => {
    return new Promise(function(resolve, reject) {
      setTimeout(function () {
        resolve({ok:true, data: require('../Fixtures/horaires.json')})
      }, 1000)
    })
  },
  getAgenda: () => {
    return new Promise(function(resolve, reject) {
      setTimeout(function () {
        resolve({ok:true, data: require('../Fixtures/agenda.json')})
      }, 1000)
    })
  },


  getLivre:(code)=>{
    return new Promise(function(resolve, reject) {
      setTimeout(function () {
        let success=false
        console.tron.log(typeof code)
        if (code == "Adada") {
          resolve({
            ok: true,
            data:{
              titre:"La vie est belle",
              code:"280652",
              dispo:true
            }
          });
        }
        if (code == "Dadada") {
          resolve({
            ok: true,
            data:{
              titre:"La vie est dure",
              code:"040687",
              dispo:false
            }
          });
        }
        else{
          resolve({
            ok: false
          })
        }
      }, 1500);
    });
  },
  bookLivre:(code,titre,userId)=>{
    return new Promise(function(resolve, reject) {
      setTimeout(function () {
        let success=false
        console.tron.log(typeof code)
        if (code == "280652") { // Pour debug
          resolve({
            ok: true,
            data:{
              dateFin:"1542895473",
              titre:titre,
              code:code,
              message:""
            },
            status:200
          });
        }
        else{
          resolve({
            ok: false,
            data:{
              dateFin:"",
              titre:titre,
              code:code,
              message:"NO_CLE"
            },
            status:401
          })
        }
      }, 1500);
    });
  },
  sendRendezVous:(destinataire,campus,priorite,modalite,objet,message,userId)=>{
    return new Promise(function(resolve, reject) {
      console.tron.log("sendRendezVous")
      setTimeout(function () {
        let success=false
        console.tron.log("Campus : "+campus)
        if (campus == 1) {
          resolve({
            ok: true
            });
          }
          else{
            resolve({
            ok: false
            })
          }
        }, 1500);
      });
  }
  // // Functions return fixtures
  // getRoot: () => {
  //   return {
  //     ok: true,
  //     data: require('../Fixtures/root.json')
  //   }
  // },
  // getRate: () => {
  //   return {
  //     ok: true,
  //     data: require('../Fixtures/rateLimit.json')
  //   }
  // },
  // getUser: (username) => {
  //   // This fixture only supports gantman or else returns skellock
  //   const gantmanData = require('../Fixtures/gantman.json')
  //   const skellockData = require('../Fixtures/skellock.json')
  //   return {
  //     ok: true,
  //     data: username.toLowerCase() === 'gantman' ? gantmanData : skellockData
  //   }
  // }
}
