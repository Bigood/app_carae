export default EditReservation = (reservations, edited_reservation) => {
  //On ne mute pas le tableau, on en créé un nouveau via map()
  return reservations.map((_reservation) =>
    _reservation.id == edited_reservation.id ? edited_reservation : _reservation
  )
}
