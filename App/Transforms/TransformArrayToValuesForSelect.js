export default TransformArrayToValuesForSelect = (array, key_value, key_label) => {
  return array.map(object => {
    if (!object.hasOwnProperty(key_value) || !object.hasOwnProperty(key_label)) {
      throw 'TransformArrayToValuesForSelect : keys inexistantes dans objet';
    }
    return {
      label : object[key_label],
      value : object[key_value]
    }
  })
}
