//Modules
import moment from 'moment'

export default AddReservationToSalle = (reservation, salles) => {
  //On va muter le tableau salles pour y inclure les résas
  // deep : true pour que les objets dans le tableau soient aussi mutables
  //asMutable est une fonction de la librairie Immutable
  let _salles = salles.asMutable({deep: true});

  let salle = _salles.find((salle) => {
    return salle.id == reservation.room_id
  })
  if (salle) {
    salle.reservations.raw.push(reservation);
  }
  //Renvoie les salles en format brut ET en sorted, via un autre transform
  return {
    salles : _salles
  }
}
