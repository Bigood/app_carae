/* ***********************************************************
* A short word on how to use this automagically generated file.
* We're often asked in the ignite gitter channel how to connect
* to a to a third party api, so we thought we'd demonstrate - but
* you should know you can use sagas for other flow control too.
*
* Other points:
*  - You'll need to add this saga to sagas/index.js
*  - This template uses the api declared in sagas/index.js, so
*    you'll need to define a constant in that file.
*************************************************************/

import { call, put } from 'redux-saga/effects'
import SuggestionActions from '../Redux/SuggestionRedux'
import UtilisateurActions from '../Redux/UtilisateurRedux'

export function * postSuggestion (api, action) {
  const { data } = action
  // make the call to the api
  const response = yield call(api.postSuggestion, data)

  if (response.ok) {
    yield put(SuggestionActions.suggestionSuccess())
  }
  else {
    if (!response.status)
      yield put(UtilisateurActions.utilisateurShowInternet())
    else if (response.status == 500)
      yield put(SuggestionActions.suggestionFailure(response.data))
    else
      yield put(SuggestionActions.suggestionFailure())
  }
}
