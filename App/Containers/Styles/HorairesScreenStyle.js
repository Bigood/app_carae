import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors, Metrics, Fonts } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  ...ApplicationStyles.components,
  main_container: {
    flex: 1,
    backgroundColor: Colors.transparent
  },
  loading_container: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.transparent
  },

  content_container: {
    flex: 1,
    //alignItems: 'center',
    //backgroundColor: 'blue'
  },
  pagecontrol_container: {
    marginTop: 5,
    marginBottom: 10,
  },

  titre_container: {
    paddingBottom: Metrics.doubleBaseMargin,
    margin: Metrics.doubleBaseMargin,
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0,0,0,0.2)',

  },

  titre_texte: {
    color: Colors.white,
    fontSize: 23,
    textAlign: 'center'
  },
  ouvert_texte: {
    color: Colors.success,
  },
  ferme_texte: {
    color: Colors.error,
  },

  jours_container: {
    flexDirection: 'row',
    marginBottom: 25,
    alignItems: 'center',
    justifyContent: 'center',
  },

  prec_suiv_container: {
    flex:1,
    alignItems: 'center'
  },
  btns_jours_container: {
    //flexDirection: 'row',
    flex: 8,
    alignItems: 'center'
  },

  precsuiv_bouton: {
    /*backgroundColor: Colors.transparent,
    marginLeft: 5,
    marginRight: 5,
    paddingTop: 4,
    paddingBottom: 4,
    paddingLeft: 15,
    paddingRight: 15*/
  },

  jour_bouton: {
    backgroundColor: Colors.white,
    borderRadius: 5,
    marginLeft: 5,
    marginRight: 5,
    paddingTop: 4,
    paddingBottom: 4,
    paddingLeft: 15,
    paddingRight: 15
  },

  jour_bouton_select: {
    backgroundColor: 'rgb(153, 204,51)',
    borderRadius: 5,
    marginLeft: 5,
    marginRight: 5,
    paddingTop: 4,
    paddingBottom: 4,
    paddingLeft: 15,
    paddingRight: 15
  },

  timeline_container: {
    paddingTop: 15,
    paddingBottom: 20
  },

  time_timeline: {
    minWidth: 100,
    marginTop: -5,
    marginRight: -8
  },
  time_text_timeline: {
    fontSize: 18,
    marginTop:4,
    color: 'white',
  },

  title_timeline: {
    fontSize: 18,
    fontWeight: 'normal',
    color: 'white',
    padding: 0,
    margin: 0
  },

  description_timeline: {
    fontSize: 14,
    color: 'white',
    paddingTop: 0,
    paddingBottom: 10
  },

  details_timeline: {
    marginTop: 0,
    paddingTop: 0,
    backgroundColor: Colors.transparent,
    marginLeft: -12,
    maxWidth:200
  },

  details_timeline2: {
    flex:1,
    flexDirection: 'row',
    backgroundColor: Colors.red,
    marginBottom: -10,
    minHeight: 60
  },


  details_barre_timeline: {
    backgroundColor: 'rgb(126, 255, 72)',
    borderRadius: 5,
    width: 5,
    paddingTop: 10,
    paddingBottom: 10,
    marginRight: 18
  },
  details_texte_timeline: {

  },
  status: {
    ...Fonts.style.normal,
    marginTop:Metrics.baseMargin,
  },
  white_block : {
    paddingVertical: 2,
    paddingHorizontal: 12,
    backgroundColor: 'rgba(255,255,255,1)',
    borderRadius: 10,
    overflow:'hidden',
  },
  with_margin_top : {
    marginTop: Metrics.baseMargin,
  },
  timeline_bar : {
    width: 30,
    height:0,
    marginVertical:4,
    borderWidth:2,
    borderRadius: 2,
    marginRight: 4,
  }
})
