import {StyleSheet} from 'react-native'
import {Fonts, Metrics, Colors} from '../../Themes/'

export default StyleSheet.create({
  applicationView: {
    flex: 1,
    backgroundColor: Colors.dark
    // backgroundColor: Colors.transparent
  },

  backgroundImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    width:'100%',
    height:'100%',
    right: 0
  },
  // backgroundImage: {
  //   width:'100%',
  //   height:'100%',
  //   flex: 1,
  //   resizeMode: 'cover', // or 'stretch'
  // },
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: Colors.background
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    fontFamily: Fonts.type.base,
    margin: Metrics.baseMargin
  },
  myImage: {
    width: 200,
    height: 200,
    alignSelf: 'center'
  }
})
