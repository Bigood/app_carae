import { StyleSheet } from 'react-native'
import { ApplicationStyles, Metrics, Colors, Fonts } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  ...ApplicationStyles.forms,
  ...ApplicationStyles.components,
  illustration: {
    height:120,
    width:120,
    resizeMode: 'cover',
    borderRadius:8,
  },
  header: {
    justifyContent:'flex-start',
    alignItems: 'flex-start',
    flexWrap: 'nowrap',
  },
  box: {
    paddingHorizontal: Metrics.doubleBaseMargin,
    paddingVertical: Metrics.smallMargin,
    flexWrap: 'wrap',
    flex:1,
  },
  informations: {
    paddingTop: Metrics.baseMargin
  },

  param_with_margin : {
    marginBottom: Metrics.baseMargin,
  },
  param_block : {
    paddingTop: Metrics.baseMargin,
    flexDirection: 'row',
    flex:0,
    // alignItems:'center',
  },
  param_text: {
    ...Fonts.style.normal,
    color: Colors.text
  },
  param_icon: {
    width:15,
    height:15,
    resizeMode:'contain',
    flex:0,
    marginRight: Metrics.baseMargin,
  },
})
