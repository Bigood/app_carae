//@flow
import React, { Component } from 'react'
import { ScrollView, Text,TextInput, View } from 'react-native'
import { connect } from 'react-redux'

import BetterKeyboardAvoidingView from '../Components/BetterKeyboardAvoidingView';
import SemanticButton from '../Components/SemanticButton'
import Picker from '../Components/Picker'

// Styles
import { Fonts } from '../Themes'
import styles from './Styles/SuggestionScreenStyle'

import SuggestionActions from '../Redux/SuggestionRedux'
import UtilisateurActions from '../Redux/UtilisateurRedux'

// import colors from '../Themes/Colors'
import Modal from '../Components/Modal'
import NavbarTitleWithIcon from '../Components/NavbarTitleWithIcon'

//Internationalisation
import { translate } from '../I18n'

class SuggestionScreen extends Component {
  static navigationOptions = ({navigation}) => ({
    //Utilisation d'un composant custom dans headerTitle (et pas title, crash sur android)
    //https://github.com/facebook/react-native/issues/10232#issuecomment-398950751
    headerTitle: <NavbarTitleWithIcon icon={navigation.state.params.icon} title={translate(`SCREEN_NAME_MAP.${navigation.state.params.screen}`)} />,
  })


  constructor (props) {
    super(props);
    //Récupération du state
    //https://github.com/react-navigation/react-navigation/issues/147#issuecomment-276853739
    this.state = {
      ...props.navigation.state.params,
      ...this.state
    };
  }

  state = {
    categorie: {
      index: null,
      value: null,
    },
    text: '',

    modalsuccess:false,
    modalfail:false,
    button_label: translate('COMMON.envoyer'),
  }
  FAIL_BUTTONS = [
    {
      text: translate('COMMON.reessayer'),
      type : 'error',
      onPress : () => {
        this.setState((partialState)=> ({
          ...partialState,
          modalfail : false
        }))
      }
    }
  ]
  SUCCESS_BUTTONS = [
    {
      text: translate('COMMON.ok'),
      type : 'success',
      onPress : () => {
        this.setState((partialState)=> ({
          ...partialState,
          modalsuccess : false
        }))
      }
    }
  ]


  postSuggestion() {
    //Si on est déjà en train d'envoyer, on ignore le touch pour éviter le double envoi
    if (this.props.sending) {
      return;
    }
    if (!this.props.utilisateur.userId) {
      return this.props.utilisateurShowLogin();
    }
    try {
        let data = {
          suggestion: {
            categorie : {
              value : this.state.categorie.value,
              label : translate(`LABEL_MAP.SUGGESTION_CATEGORIES.${this.state.categorie.value}`)
            },
            text: this.state.text,
          },
          utilisateur: this.props.utilisateur
        }

        this.props.postSuggestion(data);
      }
      catch(e) {
      this.setState({ modalfail: true, button_label: translate('COMMON.reessayer'), error: translate('ERROR.champs_requis'),});
      }
  }

  //Hook sur le changement de props
  //https://stackoverflow.com/a/44750890/1437016
  componentWillReceiveProps(props) {
    if (props.sending) {
      this.setState({ button_label : translate('COMMON.en_cours') })
    }
    else if(!props.sending){
      if (!this.state.modalfail && props.error) {
        this.setState({ modalfail: true, button_label: translate('COMMON.reessayer') })
      }
      else if (!this.state.modalsuccess) {
        this.setState({ modalsuccess: true, button_label: translate('COMMON.envoye') })
      }
    }

  }

  /** Traduit les options récupérées du redux via i18n */
  renderLocalizedTypeOptions = () => {
    let categories = translate('LABEL_MAP.SUGGESTION_CATEGORIES');
    return Object.keys(categories)
           .map((key) => ({ value: key, label: categories[key] }))
  }

  render () {
    return (
    <View style={[styles.container, {backgroundColor: this.state.color}]}>
      <View style={styles.sub_nav}><Text style={styles.sub_nav_text}>{translate('SCREEN.SUGGESTION.subtitle')}</Text></View>
      <ScrollView>
        <BetterKeyboardAvoidingView>
          <View style={styles.input_with_margin_top}>
              <Text style={[styles.label, styles.padded]}>{translate('FORM.LABEL.suggestion_categorie')}</Text>
            <Picker
              placeholder={{
                label: translate('FORM.PLACEHOLDER.suggestion_categorie'),
                value: null
              }}
              items={this.renderLocalizedTypeOptions()}
              onValueChange={(itemValue, itemIndex) => this.setState({categorie: {value: itemValue, index: itemIndex -1}})}
              value={this.state.categorieSuggestion}
            />
          </View>
          <View style={styles.input_with_margin_top}>
              <Text style={[styles.label, styles.padded]}>{translate('FORM.LABEL.suggestion')}</Text>
              <TextInput
                multiline={true}
                maxLength={2000}
                onChangeText={(text) => this.setState({text : text})}
                placeholder={translate('FORM.PLACEHOLDER.suggestion')}
                placeholderTextColor="#ffffff"
                style={[styles.multilineText, styles.input, styles.input_with_margin_top]}
              />
          </View>

          <View style={styles.actions}>
            <SemanticButton
              text={this.state.button_label}
              onPress={()=>{this.postSuggestion()} }
              type="success"
            />
          </View>
        </BetterKeyboardAvoidingView>
      </ScrollView>

      <Modal
        isVisible={this.state.modalsuccess}
        title=""
        onCancel={()=> {this.setState({modalsuccess : false})}}
        buttons={this.SUCCESS_BUTTONS}>

        <View>
          <Text style={[
            Fonts.style.h1,
            styles.centerText,
            ]}
          >
            {translate('MODAL.SUGGESTION.SUCCESS.title')}
          </Text>
          <Text style={[
            Fonts.style.h4,
            styles.centerText
            ]}
          >
            {translate('MODAL.SUGGESTION.SUCCESS.body')}
          </Text>
        </View>
      </Modal>
      {/* / MODAL ERREUR */}
      <Modal
        isVisible={this.state.modalfail}
        title=""
        onCancel={() => { this.setState({ modalfail : false})}}
        buttons={this.FAIL_BUTTONS}>

        <View>
          <Text style={[
            Fonts.style.h1,
            styles.centerText
            ]}
          >
            {translate('MODAL.SUGGESTION.ERROR.title')}
          </Text>

          <Text style={[
              Fonts.style.h3,
              styles.centerText,
              {paddingTop:30, paddingBottom:30}
              ]}>
              {this.props.error || this.state.error}
          </Text>
        </View>
      </Modal>
    </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    sending : state.suggestion.sending,
    error : state.suggestion.error,
    utilisateur : state.utilisateur,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    utilisateurShowLogin: ()=>{dispatch(UtilisateurActions.utilisateurShowLogin())},
    postSuggestion : data => {dispatch(SuggestionActions.suggestionRequest(data))}
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SuggestionScreen)
