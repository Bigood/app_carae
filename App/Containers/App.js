// @flow
import '../Config'
import DebugConfig from '../Config/DebugConfig'
import React, { Component } from 'react'
import { Provider } from 'react-redux'
import { persistStore } from 'redux-persist'
import { View, ActivityIndicator, Text } from 'react-native'

import RootContainer from './RootContainer'
import createStore from '../Redux'

import { setI18nConfig } from '../I18n'
import * as RNLocalize from "react-native-localize";

//Gestion du SplashScreen
//https://medium.com/handlebar-labs/how-to-add-a-splash-screen-to-a-react-native-app-ios-and-android-30a3cec835ae
import SplashScreen from 'react-native-splash-screen';
import styles from '../Themes/ApplicationStyles'

// create our store
const store = createStore()

/**
 * Provides an entry point into our application.  Both index.ios.js and index.android.js
 * call this component first.
 *
 * We create our Redux store here, put it into a provider and then bring in our
 * RootContainer.
 *
 * We separate like this to play nice with React Native's hot reloading.
 */
class App extends Component {
  //Gestion du SplashScreen
  //https://medium.com/handlebar-labs/how-to-add-a-splash-screen-to-a-react-native-app-ios-and-android-30a3cec835ae

  constructor() {
    super()
    this.state = { rehydrated: false }
  }
  // componentDidMount() {
  //   //Pas sûr que ce soit utile ça 
  //   RNLocalize.addEventListener("change", this.handleLocalizationChange);
  // }

  componentWillMount() {
    //Pas sûr que ce soit utile ça 
    // RNLocalize.removeEventListener("change", this.handleLocalizationChange);

    //https://github.com/rt2zz/redux-persist/blob/558a3190f11f1a8a8024dc51ab6941382dfeed1d/docs/recipes.md
    persistStore(store, {}, () => {
      //Set de la langue uniquement quand on a récupéré les valeurs persistentes
      const utilisateur_langue = store.getState().utilisateur.langue;
      console.tron.log({ "Langue forcée par l'utilisateur": utilisateur_langue, utilisateur: store.getState().utilisateur });
      setI18nConfig(utilisateur_langue); // set initial config
      
      this.setState({ rehydrated: true }, SplashScreen.hide)
    })
  }
  // //Pas sûr que ce soit utile ça 
  // handleLocalizationChange = (a,b,c) => {
  //   setI18nConfig();
  //   this.forceUpdate();
  // };
  
  render () {

    if(!this.state.rehydrated){
      return (
        <View style={styles.app_loading_view.container}>
          <ActivityIndicator size='large' />
        </View>
      )
    }

    return (
      <Provider store={store}>
        <RootContainer />
      </Provider>
    )
  }
}

// allow reactotron overlay for fast design in dev mode
export default DebugConfig.useReactotron
  ? console.tron.overlay(App)
  : App
