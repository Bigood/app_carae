//@flow
import React from 'react'
import { View, Text, TouchableOpacity, Image, ScrollView } from 'react-native'
import { connect } from 'react-redux'
import Analytics from 'appcenter-analytics';

import { Images, Fonts } from '../Themes'

import { translate } from '../I18n'

import SideMenu from './SideMenu'

// More info here: https://facebook.github.io/react-native/docs/flatlist.html

import NavbarTitleWithIcon from '../Components/NavbarTitleWithIcon'
import UtilisateurActions from '../Redux/UtilisateurRedux'

// Styles
import styles from './Styles/HomeScreenStyle'

class HomeScreen extends React.Component {

  //https://github.com/react-navigation/react-navigation/issues/1789#issuecomment-306744481
  static navigationOptions = ({navigation}) => ({
    headerTitle: <NavbarTitleWithIcon title={translate("SCREEN.HOME.title")}/>,
    //https://github.com/react-navigation/react-navigation/issues/145#issuecomment-281418095
    headerLeft: <TouchableOpacity onPress={() => {navigation.state.params.toggleMenu()}}>
                  <Image source={Images.actions_icons.light.menu} style={[styles.menu_icon]}></Image>
                </TouchableOpacity>,
    // headerRight: <Button title='Reset' onPress={() => {navigation.state.params.reset()}}></Button>
  })

  // Defaults for props
  constructor(props){
    super(props);
    Analytics.setEnabled(false);
    Analytics.trackEvent('Application ouverte');
  }

  componentDidMount(){
    this.props.navigation.setParams({ toggleMenu: this.toggleMenu });
    this.props.navigation.setParams({ reset: ()=> this.props.reset() });
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    //Mise à jour du state uniquement si la prop à changer
    console.tron.log({cgu_accepte : this.props.version_cgu_accepte})
    console.tron.log({show_cgu : this.state.show_cgu})
    //Si on a pas encore vu le tutoriel
    if (!this.props.tutoriel_est_vu && !this.state.show_launchscreen) {
      this.props.navigation.navigate('LaunchScreen', {color:'black'})
      //Flag pour empêcher d'afficher plusieurs fois les cgus (componentDidUpdate est appelé plusieurs fois d'affilé)
      this.setState({
        show_launchscreen : true
      })
    }
    //Affichahge du tutoriel après les CGUs
    if (this.props.tutoriel_est_vu && !this.state.show_cgu
      && (!this.props.version_cgu_accepte || (this.props.version_cgu_accepte != this.props.version_cgu))) {
      this.props.navigation.navigate('CguScreen', {color:'black'})

      //Flag pour empêcher d'afficher plusieurs fois les cgus (componentDidUpdate est appelé plusieurs fois d'affilé)
      this.setState({
        show_cgu : true
      })
    }

    if (this.props.utilisateur.fetching && !this.state.show_login){
      //Flag pour empêcher d'afficher plusieurs fois les cgus (componentDidUpdate est appelé plusieurs fois d'affilé)
      this.setState({
        show_login : true
      })
      this.props.navigation.navigate('LoginScreen')
    }
    else if (!this.props.utilisateur.fetching && this.state.show_login) {
      this.setState({
        show_login : false
      })
    }
  }

  state = {
    show_sidemenu: false
  }

  toggleMenu = () => {
    console.tron.log(this.state)
    Analytics.trackEvent('[Accueil] SideMenu affiché');
    this.setState(partialState => ({
      show_sidemenu : !partialState.show_sidemenu
    }), ()=> {
      console.tron.log(this.state)
    })
  }

  navigateToScreen = (item) => {
    //S'il y a besoin d'être connecté et qu'on ne l'est pas
    if (item.needs_login && !this.props.utilisateur.userId)
      this.props.utilisateurShowLogin();
    else {
      Analytics.trackEvent('[Accueil] Ecran visité', {nom : item.title});
      this.props.navigation.navigate(item.screen, item);
    }
  }

  renderRow = ({item, index}) =>(
    <View style={[styles.row, index % 2 ? styles.even : styles.odd]}>
      <View style={[styles.button_block, styles.rotate, {backgroundColor:item.color}]}>
        <TouchableOpacity onPress={() => this.navigateToScreen(item)} style={styles.button}>
          <View style={[styles.item_wrapper, styles.inverse_rotate]}>
            <Image source={Images.screen_icons[item.icon]} style={[styles.icon]}></Image>
            <Text style={[styles.item_title]}>{translate(`SCREEN_NAME_MAP.${item.screen}`)}</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  )


  onCancel = (callback) => {
    this.setState({
      show_sidemenu : false
    }, callback)
  }

  render () {
    if (this.props.error_version_app_code && this.props.error_version_app_code === "error_maj_app"){
      return (
        <View style={[styles.container, styles.padded]}>
          <Image source={Images.screen_icons.suggestion} style={{alignSelf: 'center', width: 200, height: 200}} />
          <Text style={[Fonts.style.h3, styles.padded, styles.label]}>
            {translate("SCREEN.HOME.nouvelle_version")}
          </Text>
          <Text style={[Fonts.style.h5, styles.padded, styles.label]}>
            {translate("SCREEN.HOME.nouvelle_version_maj")}
          </Text>
        </View>
      )
    }
    return (
      <View style={styles.container}>
        <ScrollView contentContainerStyle={styles.listContent}>
          {
            this.props.screens.map((item, index) => {
               return(
                 this.renderRow({item,index})
               )
             })
          }
        </ScrollView>
        <SideMenu isVisible={this.state.show_sidemenu} onCancel={this.onCancel} navigation={this.props.navigation}/>
      </View>
    )
  }
}

HomeScreen.defaultProps = {
  tutoriel_est_vu: true
}

const mapStateToProps = (state) => {
  return {
    screens: state.screen.list,
    tutoriel_est_vu:  state.utilisateur.tutoriel_est_vu,
    version_cgu_accepte: state.utilisateur.version_cgu_accepte,
    version_cgu: state.html.version_cgu,
    utilisateur : state.utilisateur,
    error_version_app_code: state.html.error_version_app_code,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    utilisateurShowLogin: ()=>{dispatch(UtilisateurActions.utilisateurShowLogin())},
    reset : ()=> {dispatch({type:'RESET'})},
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen)
