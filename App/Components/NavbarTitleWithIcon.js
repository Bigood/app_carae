import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { View, Text, Image } from 'react-native'

import styles from './Styles/NavbarTitleWithIconStyle'

import { Images } from '../Themes'

export default class NavbarTitleWithIcon extends Component {
  // Prop type warnings
  static propTypes = {
    icon: PropTypes.string,
    title: PropTypes.string.isRequired,
  }

  // Defaults for props
  static defaultProps = {
    someSetting: false
  }

  render () {
    return (
      <View style={[styles.inline]}>
        {!!this.props.icon && <Image source={Images.screen_icons[this.props.icon]} style={[styles.icon]}/>}
        <Text style={styles.title}>{this.props.title}</Text>
      </View>
    )
  }
}
