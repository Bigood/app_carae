import React, { Component } from 'react'
import PropTypes from 'prop-types';

import { HTMLStyles } from '../Themes'
import HTMLView from 'react-native-htmlview';

export default class SemanticButton extends Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    value: PropTypes.string,
    style: PropTypes.object,
  }

  trimNewLines(text) {
    if (!text) return;
    return text.replace(/(\r\n|\n|\r)/gm, '');
  }

  render () {
    return (
      <HTMLView style={this.props.style} paragraphBreak='' stylesheet={HTMLStyles} value={this.trimNewLines(this.props.value)}></HTMLView>
    )
  }
}
