//@flow
import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types';

import { View, Text, TouchableOpacity } from 'react-native'
import styles from './Styles/InternetModalStyle'
import Modal from './Modal';

import UtilisateurActions from '../Redux/UtilisateurRedux'
import SemanticButton from './SemanticButton'

class InternetModal extends Component {
  // // Prop type warnings
  // static propTypes = {
  //   isVisible: PropTypes.bool.isRequired,
  // }
  //
  // Defaults for props
  static defaultProps = {
    show_internet: false
  }

  // state = {
  //   show_internet : false
  // }

  //Options pour le modal
  //Définitions des boutons
  LOGIN_BUTTONS = [
    {
      text : 'Ok',
      type : 'warning',
      onPress : () => {
        this.props.utilisateurShowInternet()
      }
    }
  ]
  render () {
    return (
      <View>
        <Modal
          isVisible={this.props.show_internet}
          title="Vous devez être connecté à internet pour utiliser cette fonction."
          onCancel={() => this.props.utilisateurShowInternet()}
          buttons={this.LOGIN_BUTTONS}>
        </Modal>
      </View>
    )
  }
}

InternetModal.defaultProps = {
  show_internet: false
}
const mapStateToProps = (state) => {
  return {
    show_internet: state.utilisateur.show_internet
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    utilisateurShowInternet: ()=>{dispatch(UtilisateurActions.utilisateurShowInternet())}
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(InternetModal)
