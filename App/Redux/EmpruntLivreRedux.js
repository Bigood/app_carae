import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import Analytics from 'appcenter-analytics';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  empruntLivreRequest: ['data'],
  empruntLivreSuccess: ['last_book'],
  empruntLivreFailure: ['error'],
  empruntLivreReset: null,
})

export const EmpruntLivreTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  sending: null,
  error: null,
  last_book: null,
})

/* ------------- Selectors ------------- */

export const EmpruntLivreSelectors = {
  getError: state => state.error
}

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state) =>
  state.merge({ sending: true, error: null, last_book: null })

// successful api lookup
export const success = (state, action) => {
  Analytics.trackEvent('[EmpruntLivre] Réservation réussie');
  return state.merge({ sending: false, error: null, last_book : action.last_book})
}

// Something went wrong somewhere.
export const failure = (state, action) =>{
  let { error } = action;
  Analytics.trackEvent('[EmpruntLivre] Réservation échouée', error);
  return state.merge({ sending: false, error: error, last_book: null })
}

// Remise à zero du state
export const reset = (state) =>
  state.merge({ sending: false, error: null, last_book: null })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.EMPRUNT_LIVRE_REQUEST]: request,
  [Types.EMPRUNT_LIVRE_SUCCESS]: success,
  [Types.EMPRUNT_LIVRE_FAILURE]: failure,
  [Types.EMPRUNT_LIVRE_RESET]: reset,
})
