import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  htmlRequest: ['data'],
  htmlSuccess: ['payload'],
  htmlFailure: null,

  requestCgu: null,
  successCgu: ['html', 'version'],
  failureCgu: null,
  failureAppVersion: ['code', 'plaintext'],

  setHtmlDescription: ['html_data'],
  setHtmlActualites: ['html_data'],
})

export const HtmlTypes = Types
export default Creators

/* ------------- Initial State ------------- */
const DEFAULT_HTML = "<!DOCTYPE html><html><body></body></html>"

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null,

  html_description: DEFAULT_HTML,
  html_cgu: DEFAULT_HTML,
  version_cgu: null,

  fetching_cgu: null,

  error_version_app_code: null,
  error_version_app_plaintext: null,
})

/* ------------- Selectors ------------- */

export const HtmlSelectors = {
  getVersionCGU: state => state.version_cgu
}

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }) =>
  state.merge({ fetching: true, data, payload: null })

// successful api lookup
export const success = (state, action) => {
  const { payload } = action
  return state.merge({ fetching: false, error: null, payload })
}

// Something went wrong somewhere.
export const failure = state =>
  state.merge({ fetching: false, error: true, payload: null })

export const setHtmlDescription = (state, data) => {
  return state.merge({ html_description: data.html_data })
}

export const setHtmlActualites = (state, data) => {
  return state.merge({ html_actualites: data.html_data })
}


// Récupération des CGU
export const requestCgu = (state) =>
  state.merge({fetching_cgu:true})

// Les CGU ont été récupérées
export const successCgu = (state, action) => {
  const { html, version } = action
  // console.tron.log(action)
  return state.merge({ fetching_cgu: false, error: null, html_cgu: html, version_cgu: version, error_version_app_code: null, error_version_app_plaintext: null, })
}

// Il y a eu un problème lors de la récupération des CGU
export const failureCgu = state => 
  state.merge({ fetching_cgu: false, error: true, payload: null })

// Il y a eu un problème lors de la récupération des CGU
export const failureAppVersion = (state, {code, plaintext}) => {
  return state.merge({ fetching_cgu: false, error: true, error_version_app_code: code, error_version_app_plaintext: plaintext, payload: null })
}

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.HTML_REQUEST]: request,
  [Types.HTML_SUCCESS]: success,
  [Types.HTML_FAILURE]: failure,

  [Types.REQUEST_CGU]: requestCgu,
  [Types.SUCCESS_CGU]: successCgu,
  [Types.FAILURE_CGU]: failureCgu,
  [Types.FAILURE_APP_VERSION]: failureAppVersion,


  [Types.SET_HTML_DESCRIPTION]: setHtmlDescription,
  [Types.SET_HTML_ACTUALITES]: setHtmlActualites
})
