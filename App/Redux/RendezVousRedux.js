import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import Analytics from 'appcenter-analytics';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  rendezVousRequest: ['data'],
  rendezVousSuccess: null,
  rendezVousFailure: ['error']
})

export const RendezVousTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  sending: null,
  error: null
})

/* ------------- Selectors ------------- */

export const RendezVousSelectors = {
  getError: state => state.error
}

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state) =>
  state.merge({ sending: true, error: null })

// successful api lookup
export const success = (state, action) => {
  Analytics.trackEvent('[RendezVous] Envoi réussi');
  return state.merge({ sending: false, error: null})
}

// Something went wrong somewhere.
export const failure = (state, action) =>{
  let { error } = action;
  if (typeof error != "string") {
    error = JSON.stringify(error);
  }
  Analytics.trackEvent('[RendezVous] Envoi échoué', error);
  return state.merge({ sending: false, error: error })
}

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.RENDEZ_VOUS_REQUEST]: request,
  [Types.RENDEZ_VOUS_SUCCESS]: success,
  [Types.RENDEZ_VOUS_FAILURE]: failure
})
