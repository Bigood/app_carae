import Colors from './Colors'
const type = {
  base: 'Hero',
  light: 'Hero-Light',
  bold: 'Hero',
  emphasis: 'Hero'
}

const size = {
  h1: 38,
  h2: 34,
  h3: 30,
  h4: 26,
  h5: 20,
  h6: 19,
  input: 18,
  regular: 17,
  medium: 14,
  small: 12,
  tiny: 8.5
}

const style = {
  h1: {
    color: Colors.text,
    fontFamily: type.light,
    fontSize: size.h1,
    fontWeight:'normal',//https://github.com/react-navigation/react-navigation/issues/542#issuecomment-345289122
  },
  h2: {
    color: Colors.text,
    fontFamily: type.light,
    fontWeight: 'bold',
    fontSize: size.h2,
    fontWeight:'normal',//https://github.com/react-navigation/react-navigation/issues/542#issuecomment-345289122
  },
  h3: {
    color: Colors.text,
    fontFamily: type.light,
    fontSize: size.h3,
    fontWeight:'normal',//https://github.com/react-navigation/react-navigation/issues/542#issuecomment-345289122
  },
  h4: {
    color: Colors.text,
    fontFamily: type.base,
    fontSize: size.h4,
    fontWeight:'normal',//https://github.com/react-navigation/react-navigation/issues/542#issuecomment-345289122
  },
  h5: {
    color: Colors.text,
    fontFamily: type.base,
    fontSize: size.h5,
    fontWeight:'normal',//https://github.com/react-navigation/react-navigation/issues/542#issuecomment-345289122
  },
  h6: {
    color: Colors.text,
    fontFamily: type.base,
    fontSize: size.h6,
    fontWeight:'normal',//https://github.com/react-navigation/react-navigation/issues/542#issuecomment-345289122
  },
  normal: {
    color: Colors.text,
    fontFamily: type.base,
    fontSize: size.regular,
    fontWeight:'normal',//https://github.com/react-navigation/react-navigation/issues/542#issuecomment-345289122
  },
  small: {
    color: Colors.slightlyTransparentText,
    fontFamily: type.base,
    fontSize: size.small,
    fontWeight:'normal',//https://github.com/react-navigation/react-navigation/issues/542#issuecomment-345289122
  },
  description: {
    color: Colors.text,
    fontFamily: type.base,
    fontSize: size.medium,
    fontWeight:'normal',//https://github.com/react-navigation/react-navigation/issues/542#issuecomment-345289122
  }
}

export default {
  type,
  size,
  style
}
